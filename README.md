# 继电保护matlab

#### 介绍
主要构建基于matlab的继电保护原理仿真

#### 软件架构
模型软件：Matlab
编程语言：C


#### 安装教程

1.  mdl,slx为模型文件，可直接运行
2.  m文件为编程文件，先运行模型文件，再运行编程文件
3.  mat文件为数据文件

#### 参与贡献

1.  Fork 本仓库


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)

#### 感谢支持

![感谢各位支持up主](https://images.gitee.com/uploads/images/2020/1128/231630_c5e65675_7971931.png "微信图片_20200827144124.png")
